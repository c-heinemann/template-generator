# Template Generator
This is the template generator, you can use to put any template automatically in the desired folder!

# Custom Templates
If a template does not exist by default you can create your own template folder in the root of your project!

## File Structure & actual existing templates
&nbsp;.\
 ┣ 📂templates\
 ┃ ┣ 📂nuxt\
 ┃ ┃ ┣ 📜single-file-component.vue\
 ┃ ┃ ┗ 📜single-file-page.vue\
 ┃ ┣ 📂vue2\
 ┃ ┃ ┗ 📜single-file-component.vue\
 