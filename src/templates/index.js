const templates = [
    {
        framework: 'vue2',
        template: 'component',
        ending: '.vue'
    },
    {
        framework: 'vue2',
        template: 'single-file-component.vue',
        ending: '.vue'
    },
    {
        framework: 'nuxt',
        template: 'single-file-page.vue',
        ending: '.vue'
    }
]

const frameworks = [
    'vue2',
    'nuxt'
]

const questions = {
    vue2: [{
        type: 'list',
        name: 'template',
        message: 'Please choose which project template to use',
        choices: templates.filter(temp => temp.framework === 'vue2').flatMap(temp => temp.template),
        default: 'single-file-component.vue',
    }, {
        type: 'input',
        name: 'folder',
        message: 'Please choose a folder to place the template file',
        default: './components'
    }, {
        type: 'input',
        name: 'name',
        message: 'Please enter a name of the new file',
        default: 'index',
    }
    ],
    nuxt: [{
        type: 'list',
        name: 'template',
        message: 'Please choose which project template to use',
        choices: templates.filter(temp => temp.framework === 'nuxt').flatMap(temp => temp.template),
        default: 'single-file-page.vue',
    }, {
        type: 'input',
        name: 'folder',
        message: 'Please choose a folder to place the template file'
    }, {
        type: 'input',
        name: 'name',
        message: 'Please enter a name of the new file',
        default: 'index',
    }
    ],
    angular: []
}

export default { templates, frameworks, questions };