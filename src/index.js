import chalk from 'chalk';
import fs from 'fs';
import ncp from 'ncp';
import path from 'path';
import { promisify } from 'util';
import Listr from 'listr'
import makeDir from 'make-dir'

const access = promisify(fs.access);
const copy = promisify(ncp);

async function copyTemplateFiles(options) {
    return copy(options.templateDirectory, options.targetDirectory, {
        clobber: false,
    });
}

export async function createTemplate(options) {
    options = {
        ...options,
        targetDirectory: options.folder + '\\' + options.name
    };

    const templateDir = path.resolve(
        './src/templates/',
        options.framework.toLowerCase() + '/',
        options.template.toLowerCase()
    );
    

    if(!fs.existsSync(templateDir)){
        path.resolve(
            './templates/',
            options.framework.toLowerCase() + '/',
            options.template.toLowerCase()
        );
        if(!fs.existsSync(templateDir)){
            console.error(chalk.red.bold('Template does not exists!'))
        }
    } else {
        options.templateDirectory = templateDir;
    }


    try {
        await access(templateDir, fs.constants.R_OK);
    } catch (err) {
        console.error('%s Invalid template name', chalk.red.bold('ERROR'));
        process.exit(1);
    }

    const tasks = new Listr([
        {
            title: 'Copy template files',
            task: () => copyTemplateFiles(options)
                .then(() => console.log(chalk.green.bold('Copy Success')))
                .catch(err => {
                    makeDir(options.folder)
                    copyTemplateFiles(options)
                        .then(() => console.log(chalk.green.bold('Copy Success')))
                        .catch(err => console.error(chalk.red.bold('Copy Failed')))
                }),
        }])

    await tasks.run()

    console.log('%s Project ready', chalk.green.bold('DONE'));

    return true;
}

function resolveNewTemplateFolder(options) {
    const currentFileUrl = path.resolve('./')
    console.log(currentFileUrl)
    if (options.framework === 'vue') {
        switch (options.template) {
            case 'single-file-component.vue':
                return currentFileUrl + '\\components'

        }
    }

    if (options.framework === 'nuxt') {
        switch (options.template) {
            case 'single-file-page.vue':
                return currentFileUrl + '\\pages'
            case 'single-file-component.vue':
                return currentFileUrl + '\\components'
            default:
                return currentFileUrl
        }
    }
}