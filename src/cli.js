import templates from './templates'
import arg from 'arg'
import inquirer from 'inquirer'
import { createTemplate } from './index'
import path from 'path'

export async function cli(args) {
    let options = parseArgumentsIntoOptions(args)
    options = await promptForMissingOptions(options);
    console.log(options)
    await createTemplate(options)
}


function parseArgumentsIntoOptions(rawArgs) {
    const args = arg(
        {
            '--framework': String,
            '-f': '--framework',
            '--template': String,
            '-t': '--template',
            '--dir': String,
            '-d': '--dir'
        },
        {
            argv: rawArgs.slice(2),
        }
    );
    return {
        template: args['--template'],
        framework: args['--framework'],
        folder: args['--dir'],
        name: args._[0]
    };
}

async function promptForMissingOptions(options) {
    let questions = [];
    if (!options.framework) {
        questions.push({
            type: 'list',
            name: 'framework',
            message: 'Please choose which project template to use',
            choices: templates.frameworks,
        });
    }

    let answers = await inquirer.prompt(questions);
    let framework = answers.framework || options.framework
    questions = templates.questions[framework].filter(q => !options[q.name])

    let answers2 = await inquirer.prompt(questions);
    let temp = templates.templates.find(t => t.framework === framework)
    return {
        ...options,
        template: options.template || answers2.template,
        framework: options.framework || answers.framework,
        folder: options.folder || path.resolve(answers2.folder),
        name: (options.name || answers2.name) + ( temp ? temp.ending : '.js' )
    };
}